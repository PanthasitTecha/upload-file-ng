var express = require('express');
var app = express();
var mongojs = require('mongojs');
var bodyParser = require('body-parser');
var db = mongojs('SITDRIVE_DB', ['file_collections']);
var path = require('path');
var multer = require('multer');
var fs = require('fs');

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

//------------------get container list------------------------
app.get('/containerlist', function (req, res) {
    console.log("I received a Get request")
    db.file_collections.find(function (err, docs) {
        console.log(docs);
        res.json(docs);
    })
});
//--------------------------------------------------------------




//-------------------delete file--------------------------------
app.delete('/containerlist/:id',function(req,res){
    var id = req.params.id;
    console.log(id +" has been removed");
    db.file_collections.remove({_id:mongojs.ObjectId(id)},function(err,docs){
        res.json(docs);
    });
});
//----------------------------------------------------------------




//--------------------upload file module---------------------------------
var dir = './uploads'
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        callback(null, './uploads');
    },
    filename: function (req, file, callback) {
        var filename = file.originalname;
        var fileExtension = filename;
        callback(null, filename);
        console.log(filename);

    }
});
//------------------------------------------------------------------




//--------------------upload file routing---------------------------
var upload = multer({ storage: storage }).single('file');
app.post('/uploadfile', function (req, res) {

    upload(req, res, function (err) {
        if (err) {
            console.log(err)
            res.end("Error uploading file.");
        }
        var file_name = req.file.filename;
        var file_size = req.file.size;
        var fileData = new FILE_COLLECTION();
        fileData.filename = file_name;
        fileData.size = file_size;
        fileData.save(function (err, saveObject) {
            if (err) {
                console.log(err);
                res.status(500).send();
            } else {
                res.send(saveObject);
            }
        });
        console.log(file_name + file_size)
    });
});
//------------------------------------------------------------------




//----------------------------Create folder routing-----------------
app.post('/createFolder', function (req, res) {
    var folder = String(req.body.name);
    console.log("Created :" + folder)
    res.send(fs.mkdirSync(folder));
});

//------------------------------------------------------------------




app.listen(3000);
console.log("Server running on port 3000");