function AppCtrl($scope, $http) {
    console.log("Hello from controller")

    var refresh = function () {

        $http.get('/containerlist').success(function (response) {
            console.log("I got data I requested");
            $scope.containerlist = response;
            $scope.file = "";
        });
    };

    refresh();

    $scope.remove = function(id){
        console.log(id);
        $http.delete('/containerlist/'+id).success(function(response){
            refresh();
        })
    };

    $scope.uploadFile = function(){
        console.log("User uploadfile");
        $http.post('/uploadfile').success(function(response){
            refresh();
        })
    };
}